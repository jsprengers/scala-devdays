import devdays._
import devdays.actor.{AkkaConfig, ListDispatcherActor}
import org.scalatest.FunSuite

class AddressValidatorAkkaTest extends FunSuite {


  val correct = AddressInput("5941ED", street = "Sparrenlaan 6", town = "Velden")
  val unknownPostcode = AddressInput("3011BE", street = "Zuidhoek 1", town = "Rotterdam")

  test("test validation for list") {
    val addresses: List[AddressInput] = 1.to(20).map(i => AddressInput("5941ED",s"Sparrenlaan $i","Velden")).toList
    AkkaConfig.managerActor ! addresses
    Thread.sleep(20000)

  }

}
