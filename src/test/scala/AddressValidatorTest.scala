import devdays._
import org.scalatest.FunSuite
import org.scalatest.Matchers._

import scala.util.{Failure, Success}

class AddressValidatorTest extends FunSuite {

  val validator = new AddressValidatorConcise(new MockPostcodeDB)

  val correct = AddressInput("5941ED", street = "Sparrenlaan 6", town = "Velden")
  val unknownPostcode = AddressInput("3011BE", street = "Zuidhoek 1", town = "Rotterdam")
  val numberMissing = AddressInput("5941ED", street = "Sparrenlaan", town = "Velden")
  val numberFormatWrong = AddressInput("5941ED", street = "Sparrenlaan 12ABC", town = "Velden")

  val correctStreet = StreetAndNumber("Sparrenlaan", "6")
  val correctTown = Success(PostcodeMatch("Sparrenlaan", "Velden"))
  val lookupFailed = Failure(new IllegalArgumentException("lookup failed"))

  test("test validation for list") {
    val addresses = List(correct, unknownPostcode, numberFormatWrong, numberMissing)
    val (ok, nok) = validator.validate(addresses)
    ok should have size 1
    nok should have size 3
  }

  test("test validation for single entry") {
    assert(validator.validate(correct).isLeft)
    assert(validator.validate(unknownPostcode).isRight)
    assert(validator.validate(numberMissing).isRight)
    assert(validator.validate(numberFormatWrong).isRight)
  }

  test("look up valid postcode") {
    assert(validator.lookupPostcode("5941ED").isSuccess)
  }

  test("look up invalid postcode") {
    assert(validator.lookupPostcode("3011BB").isFailure)
  }

  test("Beukenlaan 5 is a correct address") {
    assert(validator.parseAddress("Beukenlaan 5").isSuccess)
  }

  test("Beukenlaan 12AB is malformed") {
    assert(validator.parseAddress("Beukenlaan 12AB").failed.get.getMessage == "Invalid address: Beukenlaan 12AB")
  }

  test("Address Beukenlaan misses number") {
    assert(validator.parseAddress("Beukenlaan").failed.get.getMessage == "Invalid address: Beukenlaan")
  }

  test("Correct postcode and street should validate ok") {
    val result = validator.validateForPostCode(correct, correctStreet)
    assert(result.isLeft)
  }

  test("Town from postcode lookup does not match original input") {
    val result = validator.validateForPostCode(AddressInput("5941ED", street = "Beukenlaan 6", town = "Velden"), StreetAndNumber("Beukenlaan", "6"))
    assert(result.right.get.error == "Street or town do not match postcode lookup")
  }

  test("postcode lookup does not match") {
    val result = validator.validateForPostCode(unknownPostcode, StreetAndNumber("Zuidhoek", "1"))
    assert(result.isRight)
  }

}
