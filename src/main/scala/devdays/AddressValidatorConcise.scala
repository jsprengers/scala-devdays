package devdays

import scala.util.Either.RightProjection
import scala.util.{Failure, Success, Try}

class AddressValidatorConcise(postcodeDB: PostcodeDB) {

  private val parser = new AddressLineParserImpl

  def validate(addresses: List[AddressInput]): (List[ValidatedAddress], List[InvalidAddress]) = {
    val (okAddresses, malformedAddresses) = bimap[AddressInput, (AddressInput, StreetAndNumber), InvalidAddress](addresses, a => {
      parseAddress(a.street) match {
        case Failure(e) => Right(InvalidAddress(a, e.getMessage))
        case Success(street) => Left((a, street))
      }
    })
    val (postcodesOK, postcodesFailed) = bimap[(AddressInput, StreetAndNumber), ValidatedAddress, InvalidAddress](okAddresses, a => validateForPostCode(a._1, a._2))
    (postcodesOK, malformedAddresses ++ postcodesFailed)
  }

  def validate(address: AddressInput): Either[ValidatedAddress, InvalidAddress] = {
    val firstCheck = parseAddress(address.street) match {
      case Failure(e) => Right(InvalidAddress(address, e.getMessage))
      case Success(street) => Left((address, street))
    }
    firstCheck match {
      case Right(invalid) => Right(invalid)
      case Left((address, streetAndNumber)) => validateForPostCode(address, streetAndNumber)
    }
  }

  def parseAddress(str: String): Try[StreetAndNumber] = Try(parser.parse(str))

  def lookupPostcode(str: String): Try[PostcodeMatch] = Try(postcodeDB.lookup(str))

  def validateForPostCode(address: AddressInput, streetNumber: StreetAndNumber): Either[ValidatedAddress, InvalidAddress] = {
    lookupPostcode(address.postcode) match {
      case Failure(e) => Right(InvalidAddress(address, e.getMessage))
      case Success(lookup) => if (!streetNumber.street.equalsIgnoreCase(lookup.street) || !address.town.equalsIgnoreCase(lookup.town))
        Right(InvalidAddress(address, "Street or town do not match postcode lookup"))
      else Left(ValidatedAddress(address.postcode, lookup.street, streetNumber.number, lookup.town))
    }
  }

  def bimap[I, L, R](in: List[I], splitter: I => Either[L, R]): (List[L], List[R]) = {
    val (left, right) = in.map(el => splitter(el)).partition(_.isLeft)
    (left.map(_.left.get), right.map(_.right.get))
  }

}
