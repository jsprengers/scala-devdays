package devdays

class AddressLineParserImpl extends AddressLineParser {

  def parse(street: String): StreetAndNumber = {
    val splitter = "^(.*?) (\\d+[A-Z]?)$".r
    val matched = splitter.findAllIn(street)
    if (matched.hasNext && matched.groupCount == 2)
      StreetAndNumber(matched.group(1), matched.group(2))
    else throw new IllegalArgumentException(s"Invalid address: $street")
  }

}
