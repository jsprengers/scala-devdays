package devdays

class MockPostcodeDB extends PostcodeDB {

  def lookup(str: String): PostcodeMatch = str match {
    case "5941ED" => PostcodeMatch("Sparrenlaan", "Velden")
    case "5591HA" => PostcodeMatch("Molenstraat", "Heeze")
     case _ => throw new IllegalArgumentException(s"Postcode $str unknown")
  }

}
