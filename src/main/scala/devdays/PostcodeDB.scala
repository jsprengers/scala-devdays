package devdays

trait PostcodeDB {
  def lookup(str: String): PostcodeMatch
}
