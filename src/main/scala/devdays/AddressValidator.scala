package devdays

import scala.util.{Failure, Success, Try}

class AddressValidator(postcodeDB: PostcodeDB) {

  private val parser = new AddressLineParserImpl

  type Validation = (Try[PostcodeMatch], Try[StreetAndNumber])

  def validate(addresses: List[AddressInput]): (List[ValidatedAddress], List[InvalidAddress]) = {
    val postcodeValidations = addresses.map(a => lookupPostcode(a.postcode))
    val addressLineValidations = addresses.map(a => parseAddress(a.street))
    val inputWithValidationResults: List[(AddressInput, (Try[PostcodeMatch], Try[StreetAndNumber]))] = zipValidationResults(addresses, postcodeValidations, addressLineValidations)
    val executedValidations: List[Either[ValidatedAddress, InvalidAddress]] = executeValidations(inputWithValidationResults)
    partitionResults(executedValidations)
  }

  def validate(address: AddressInput): Either[ValidatedAddress, InvalidAddress] = {
    executeValidations(List((address, (lookupPostcode(address.postcode), parseAddress(address.street))))) match {
      case Nil => throw new IllegalStateException("")
      case head :: tail => head
    }
  }

  def lookupPostcode(postcode: String): Try[PostcodeMatch] = Try(postcodeDB.lookup(postcode))

  def parseAddress(addressLine: String): Try[StreetAndNumber] = Try(parser.parse(addressLine))

  def zipValidationResults(addresses: List[AddressInput],
                           postcodeValidations: List[Try[PostcodeMatch]],
                           streetValidations: List[Try[StreetAndNumber]]): List[(AddressInput, Validation)] =
    addresses.zip(postcodeValidations.zip(streetValidations))

  def executeValidations(addressWithValidations: List[(AddressInput, Validation)]): List[Either[ValidatedAddress, InvalidAddress]] =
    addressWithValidations.map(addressWithValidation => {
      val (address, results) = addressWithValidation
      val (streetAndTown, streetAndNumber) = results
      validateAddress(address, streetAndTown, streetAndNumber)
    })

  def validateAddress(address: AddressInput,
                      streetAndTown: Try[PostcodeMatch],
                      streetAndNumber: Try[StreetAndNumber]
                      ): Either[ValidatedAddress, InvalidAddress] = {
    def reject(message: String) = Right(InvalidAddress(address, message))

    streetAndTown match {
      case Failure(err) => reject(err.getMessage)
      case Success(sat) => streetAndNumber match {
        case Failure(err) => reject(err.getMessage)
        case Success(number) => if (number.street != sat.street || address.town != sat.town)
          reject("Street or town do not match postcode lookup")
        else Left(ValidatedAddress(address.postcode, sat.street, number.number, sat.town))
      }
    }
  }

  def partitionResults(validationresults: List[Either[ValidatedAddress, InvalidAddress]]): (List[ValidatedAddress], List[InvalidAddress]) = {
    val (validated, nonValidated) = validationresults.partition(_.isLeft)
    (validated.map(_.left.get), nonValidated.map(_.right.get))
  }
}
