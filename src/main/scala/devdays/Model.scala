package devdays


case class AddressInput(postcode: String,
                        street: String = "",
                        town: String = "")

case class InvalidAddress(address: AddressInput, error: String)

case class ValidatedAddress(postcode: String,
                            street: String,
                            number: String,
                            town: String)

case class StreetAndNumber(street: String, number: String)

case class PostcodeMatch(street: String, town: String)
