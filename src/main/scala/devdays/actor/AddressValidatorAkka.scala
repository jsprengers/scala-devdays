package devdays.actor

import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.dispatch.MessageDispatcher
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}
import devdays._

import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

class AddressValidatorAkka() {
  val conf1: Config = ConfigFactory.load(ConfigFactory.parseString(
    """
    my-dispatcher {
        type = Dispatcher
        executor = "thread-pool-executor"
        thread-pool-executor {
          fixed-pool-size = 32
        }
        throughput = 1
  }
 """))
  val system = ActorSystem("ValidatorSystem", conf1)
  implicit val executionContext: MessageDispatcher = system.dispatchers.lookup("my-dispatcher")
  // default Actor constructor
  val postcodeActor: ActorRef = system.actorOf(Props[AddressValidatorActor].withDispatcher("my-dispatcher"), name = "postcode")

  implicit val timeout = Timeout(Duration.create(3, TimeUnit.SECONDS))

  def validate(addresses: List[AddressInput]): (List[ValidatedAddress], List[InvalidAddress]) = {
    addresses.foreach(address => postcodeActor ! address)
    println("All dispatched. Awaiting results")
    Thread.sleep(10000)
    (List(), List())
  }

  private def lift[T](futures: List[Future[T]]) =
    futures.map(_.map {
      Success(_)
    }.recover { case t => Failure(t) })

  def waitAll[T](futures: List[Future[T]]): Future[List[Try[T]]] =
    Future.sequence(lift(futures)) // having neutralized exception completions through the lifting, .sequence can now be used

  def partitionResults(validationresults: List[Either[ValidatedAddress, InvalidAddress]]): (List[ValidatedAddress], List[InvalidAddress]) = {
    val (validated, nonValidated) = validationresults.partition(_.isLeft)
    (validated.map(_.left.get), nonValidated.map(_.right.get))
  }

}
