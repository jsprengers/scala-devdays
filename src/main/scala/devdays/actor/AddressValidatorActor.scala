package devdays.actor

import akka.actor.Actor
import devdays.{AddressInput, AddressValidator, MockPostcodeDB}

import scala.util.Random

class AddressValidatorActor extends Actor{

  val validator = new AddressValidator(new MockPostcodeDB)
  val random = new Random
  override def receive: Receive = {
    case a: AddressInput =>
      val id = Thread.currentThread().getId
      println(s"$id looking up ${a.street}")
      Thread.sleep(5 + random.nextInt(200) )
      sender ! validator.validate(a)
      println(s"$id Found  ${a.street}")
    case _  => sender ! "Huh?"
  }

  def now: Long = System.nanoTime() / 1000000
}
