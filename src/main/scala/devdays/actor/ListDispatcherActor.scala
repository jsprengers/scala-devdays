package devdays.actor

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.dispatch.MessageDispatcher
import com.typesafe.config.{Config, ConfigFactory}
import devdays.actor.AkkaConfig.system
import devdays.{AddressInput, InvalidAddress, ValidatedAddress}

import scala.collection.mutable.ListBuffer

class ManagerActor extends Actor {

  implicit val executionContext: MessageDispatcher = AkkaConfig.dispatcher

  override def receive: Receive = {
    case addresses: List[AddressInput] =>
      println(s"sending ${addresses.size} addresses")
      AkkaConfig.listDispatcherActor ! addresses
    case ret: List[Either[ValidatedAddress, InvalidAddress]] =>
      println("Succeeded!")
  }
}

class ListDispatcherActor extends Actor {

  val buffer = ListBuffer[Either[ValidatedAddress, InvalidAddress]]()
  implicit val executionContext: MessageDispatcher = AkkaConfig.dispatcher

  override def receive: Receive = {
    case addresses: List[AddressInput] =>
      println(s"Processing ${addresses.size} addresses")
      addresses.foreach(address => AkkaConfig.postcodeActor ! address)
    case ret: Either[ValidatedAddress, InvalidAddress] =>
      buffer.append(ret)
      if ( buffer.size == 20 ) AkkaConfig.managerActor ! buffer.toList
  }
}

object AkkaConfig {
  val conf1: Config = ConfigFactory.load(ConfigFactory.parseString(
    """
    my-dispatcher {
        type = Dispatcher
        executor = "thread-pool-executor"
        thread-pool-executor {
          fixed-pool-size = 32
        }
        throughput = 1
  }
 """))
  val system = ActorSystem("ValidatorSystem", conf1)
  val dispatcher = system.dispatchers.lookup("my-dispatcher")
  val managerActor: ActorRef = system.actorOf(Props[ManagerActor], name = "manager")
  val listDispatcherActor = system.actorOf(Props[ListDispatcherActor], name = "listDispatcher")
  val postcodeActor = system.actorOf(Props[AddressValidatorActor].withDispatcher("my-dispatcher"), name = "postcode")
}
