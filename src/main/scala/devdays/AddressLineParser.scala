package devdays

trait AddressLineParser {
  def parse(street: String) : StreetAndNumber
}
