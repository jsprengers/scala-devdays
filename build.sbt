name := "devdaysdemo"

version := "0.1"

scalaVersion := "2.12.4"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-reflect" % scalaVersion.value,
  "org.scala-lang" % "scala-compiler" % scalaVersion.value,
  "org.scalatest" %% "scalatest" % "3.0.4" % "test",
  "com.typesafe.akka" % "akka-actor_2.12" % "2.5.26",
  "com.typesafe.akka" % "akka-dispatch_2.12" % "2.5.26",
  "com.typesafe.akka" %% "akka-stream" % "2.5.26")
